<service xmlns="http://telepathy.freedesktop.org/wiki/service-profile-v1"
         id="sofiasip-sip"
         type="IM"
         manager="sofiasip"
         protocol="sip"
         icon="voicecall">
  <name>Voice over IP (VoIP/SIP)</name>
</service>
